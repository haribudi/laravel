-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2015 at 03:54 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sampleapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `email`, `created_at`, `updated_at`) VALUES
(2, '$2y$10$5BskX6w0qaZQUdWFyTXsi.A2tNw09pYExIb/JrIJcfmcV2cdFmlL6', 'fitriagriyahijab@gmail.com', '2015-02-03 06:34:03', '2015-02-03 06:34:03'),
(6, '$2y$10$ykEG2G.BURVne4UpyK9O5e0AApMLv2lTuXNIotXHV0UisJL4K5Osq', 'harbudsan@gmail.com', '2015-02-03 06:50:35', '2015-02-03 06:50:35'),
(8, '$2y$10$iNGFokjhUzcI4xha4XDCXuPiMZDastcNg1MqRNgvTWtGsIS4LK6F2', 'adyobams@rocketmail.com', '2015-02-03 06:58:20', '2015-02-03 06:58:20'),
(9, '$2y$10$hQkLD1o7JYrx3kZYKs3tl.QqSXjHpVXrD7g95FZ4vzbxTe1PZoe3y', 'talitakum.polinema@gmail.com', '2015-02-03 06:59:37', '2015-02-03 06:59:37');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
