<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', function()
{
	return View::make('hello');
});
Route::get('/register', function()
{
	return View::make('register');
});
Route::get('/namespace', function()
{
	return View::make('\namespace\namespace');
});
Route::post('/register', function()
{
//	1. setting validasi
	$validator = Validator::make(
		Input::all(),
		array(
			"email" => "required|email|unique:users,email",
			"password" => "required|min:6",
			"password_confirmation" => "same:password",
		)	
	);
	
//	2a. jika semua validasi terpenuhi
	if($validator->passes()){
		$user = new User;
		$user->email = Input::get('email');
		$user->password = Hash::make(Input::get('password'));
		$user->save();	
	
		return Redirect::to("register")->with('register_success', 'Selamat, Anda telah berhasil mendaftar, silakan cek email untuk aktivasi');
	}
//	2a. jika tidak, kembali ke form registrasi
	else{
		return Redirect::to('register')
			->withErrors($validator)
			->withInput();
	}
});
